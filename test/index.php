<?php
$host = 'localhost';
$port = 5432;
$dbname = 'postgres';
$user = 'postgres';
$password = '123';
$dsn = "pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password";

try {
    $pdo = new PDO($dsn);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

$email = 'ivana@example.com'; // Unesite email za provjeru
$password = 'lozinka2'; // Unesite lozinku za provjeru

$sql = "SELECT * FROM public.a_users WHERE email = :email AND password = :password";
$stmt = $pdo->prepare($sql);
$stmt->execute(['email' => $email, 'password' => $password]);
$result = $stmt->fetch(PDO::FETCH_ASSOC);

if ($result) {
    echo "Email i lozinka su ispravni.<br>";
} else {
    echo "Email ili lozinka nisu ispravni.<br>";
}

function getUserApplications($userId)
{
    global $pdo;

    $sql = "SELECT aa.id, aa.name, ad.get, ad.put, ad.post, ad.del
        FROM a_applications AS aa
        JOIN a_application_roles AS ar ON ar.idapplication = aa.id
        JOIN a_roles_details AS ad ON ad.idrole = ar.idrole
        WHERE ad.iduser = :userId
        AND ad.valid_from <= current_date
        AND ad.valid_until >= current_date";


    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return json_encode($result);
}

$userId = $result["id"]; // ID korisnika za kojeg želite dohvatiti aplikacije
$applicationsJson = getUserApplications($userId);
echo $applicationsJson;

$sql = "SELECT * FROM public.a_users";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Convert the array to a JSON string
$json = json_encode($result);

// Set the response headers to indicate that this is JSON data
header('Content-Type: application/json');

echo $json;
?>