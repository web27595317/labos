<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Test</title>
</head>

<body>
    <?php
    $header = '<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>';

    $footer = '</body>
    </html>';


    echo $header;

    $Izraz = "Zemlja je";
    $oblik = "okrugla";

    echo "{$Izraz} {$oblik}";
    echo "<br>";
    echo "{$Izraz} {$oblik}";


    echo $footer;

    // zadatak 9
    
    echo "<br>";
    $firma = "Markovi Motori d.o.o";
    $ulica = "Kolodvorska";
    $iznos = "50000";
    $datum = "1. travnja 2023.";
    $institucija = "PP Bjelovar";
    $ime = "Pero";
    $prezime = "Perić";
    $titula = "pjevać, gurman i bivši narkoman";

    $tekst = "Poštovani, molimo Vas da se javite u najbližu poslovnicu {$firma} na adresi {$ulica} zbog dugovanja
    od {$iznos} kn. Ako ne podmirite obveze do #{$datum} bit ćemo prisiljeni angažirati {$institucija}
    Lp, {$ime} {$prezime} {$titula}.";

    echo "{$tekst}";

    // zadatak 10
    
    ?>
</body>

</html>