<?php
$color = 'white';
$background_color = 'black';
$font_size = '80px';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CSS</title>
    <style>
        p {
            text-align: center;
            color:
                <?php echo $color; ?>
            ;
            font-size:
                <?php echo $font_size; ?>
            ;
            background-color:
                <?php echo $background_color; ?>
            ;
        }
    </style>
</head>

<body>

    <p>Stil ovog paragrafa je izgeneriran putem php skripte</p>

</body>

</html>