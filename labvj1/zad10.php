<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Tablica</title>
</head>

<body>
	<table>
		<tr>
			<th>Ime</th>
			<th>Prezime</th>
			<th>Predmet</th>
			<th>Ocjena</th>
		</tr>
		<tr>
			<td>
				<?php $ime = "Ivan";
				echo $ime; ?>
			</td>
			<td>
				<?php $prezime = "Ivic";
				echo $prezime; ?>
			</td>
			<td>
				<?php $predmet = "Java programiranje";
				echo $predmet; ?>
			</td>
			<td>
				<?php $ocjena = 4;
				echo $ocjena; ?>
			</td>
		</tr>
		<tr>
			<td>
				<?php $ime = "Ana";
				echo $ime; ?>
			</td>
			<td>
				<?php $prezime = "Anic";
				echo $prezime; ?>
			</td>
			<td>
				<?php $predmet = "C# Programiranje";
				echo $predmet; ?>
			</td>
			<td>
				<?php $ocjena = 5;
				echo $ocjena; ?>
			</td>
		</tr>
		<tr>
			<td>
				<?php $ime = "Petar";
				echo $ime; ?>
			</td>
			<td>
				<?php $prezime = "Petarovic";
				echo $prezime; ?>
			</td>
			<td>
				<?php $predmet = "Digitalna tehnika";
				echo $predmet; ?>
			</td>
			<td>
				<?php $ocjena = 5;
				echo $ocjena; ?>
			</td>
		</tr>
	</table>

	<?php
	echo "<br>";
	$pocetno_slovo = 'A';
	$sql = "SELECT * FROM KORISNICI WHERE PREZIME LIKE '$pocetno_slovo'";

	echo "{$sql}";
	?>

</body>

</html>