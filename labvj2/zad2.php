<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $predmeti = array(
        0 => 'Mikroračunala',
        1 => 'Programsko inžinjerstvo',
        2 => 'Operacijski sustavi',
        3 => 'Osnove programskog jezika JAVA',
        4 => 'C# programiranje',
        5 => 'Web programiranje 2',
        6 => 'Tehnički engleski 4'
    );

    echo "<pre>";
    print_r($predmeti);
    echo "<pre>";

    $temp = $predmeti[1];
    $predmeti[1] = $predmeti[5];
    $predmeti[5] = $temp;

    echo "<pre>";
    print_r($predmeti);
    echo "<pre>";
    ?>

</body>

</html>