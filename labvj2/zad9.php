<!DOCTYPE HTML>
<html>

<body>
    <form action="#" method="post">
        <label for="currency">Odaberite valutu:</label>
        <select name="currency" id="currency">
            <option value="036">AUD - Australija</option>
            <option value="124">CAD - Kanada</option>
            <option value="203">CZK - Češka</option>
            <option value="208">DKK - Danska</option>
            <option value="348">HUF - Mađarska</option>
            <option value="392">JPY - Japan</option>
            <option value="578">NOK - Norveška</option>
            <option value="752">SEK - Švedska</option>
            <option value="756">CHF - Švicarska</option>
            <option value="826">GBP - Britanija</option>
            <option value="840">USD - SAD</option>
            <option value="977">BAM - BiH</option>
            <option value="978">EUR - EMU</option>
            <option value="985">PLN - Poljska</option>
        </select>
        <input type="submit">
    </form>
    <?php
    $valute = array(
        "036" => "AUD - Australija",
        "124" => "CAD - Kanada",
        "203" => "CZK - Češka",
        "208" => "DKK - Danska",
        "348" => "HUF - Mađarska",
        "392" => "JPY - Japan",
        "578" => "NOK - Norveška",
        "752" => "SEK - Švedska",
        "756" => "CHF - Švicarska",
        "826" => "GBP - Britanija",
        "840" => "USD - SAD",
        "977" => "BAM - BiH",
        "978" => "EUR - EMU",
        "985" => "PLN - Poljska"
    );
    if (isset($_POST["currency"])) {
        $sifra = $_POST["currency"];
        if (array_key_exists($sifra, $valute)) {
            echo "Oznaka valute i država: " . $valute[$sifra];
        } else {
            echo "Nepoznata šifra valute. Molimo provjerite unos.";
        }
    }
    ?>
</body>

</html>