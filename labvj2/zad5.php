<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php

    $godina = array(
        1 => 31,
        2 => 28,
        3 => 31,
        4 => 30,
        5 => 31,
        6 => 30,
        7 => 31,
        8 => 31,
        9 => 30,
        10 => 31,
        11 => 30,
        12 => 31
    );

    $prijestupna = array(
        1 => 31,
        2 => 29,
        3 => 31,
        4 => 30,
        5 => 31,
        6 => 30,
        7 => 31,
        8 => 31,
        9 => 30,
        10 => 31,
        11 => 30,
        12 => 31
    );

    $mjesec = 2;
    $odabrana_godina = 2024;

    if (($odabrana_godina % 4 == 0 && $odabrana_godina % 100 != 0) || $odabrana_godina % 400 == 0) {
        $dani_u_mjesecu = $prijestupna[$mjesec];
    } else {
        $dani_u_mjesecu = $godina[$mjesec];
    }

    echo "U mjesecu " . $mjesec . " godine " . $odabrana_godina . " ima " . $dani_u_mjesecu . " dana.";

    ?>

</body>

</html>