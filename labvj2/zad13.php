<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php

    $tjedan = array(
        0 => 'Ponedjeljak',
        1 => 'Utorak',
        2 => 'Srijeda',
        3 => 'Četvrtak',
        4 => 'Petak',
        5 => 'Subota',
        6 => 'Nedjelja'
    );

    foreach ($tjedan as $key => $dan) {
        echo "<pre>";
        echo "<tr><td>" . ($key + 1) . ". - </td><td>" . $dan . "</td></tr>";
        echo "<br>";
        echo "<pre>";
    }
    echo "</tbody>";
    echo "</table>";
    ?>

</body>

</html>