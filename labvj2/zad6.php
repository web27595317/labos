<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $gradovi = array(
        'Hrvatska' => 'Zagreb',
        'Njemačka' => 'Berlin',
        'Austrija' => 'Beč',
        'Francuska' => 'Pariz',
        'Mađarska' => 'Budimpešta'
    );

    $drzava = 'Njemačka';

    echo "<pre>";
    print_r($gradovi);
    echo "<pre>";

    echo "<pre>";
    echo "$drzava: ";
    echo "$gradovi[$drzava]";
    echo "<pre>";

    ?>

</body>

</html>