<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="get">
        <label for="username">Unesite vaš username: </label>
        <input type="text" id="username" name="username"><br>
        <button type="submit" name="submit">Pošalji</button>
    </form>
    <script>
        function showAlert(username) {
            if (username) {
                alert("Pozdrav, " + username);
                return 0;
            } else {
                alert("Molimo unesite username");
                return 0;
            }
        }
    </script>
    <?php
    if (isset($_GET['submit'])) {
        if (empty($_GET['username'])) {
            echo '<script>showAlert()</script>';
        } else {
            $username = $_GET['username'];
            echo '<script>showAlert("' . $username . '")</script>';
        }
    }
    ?>
</body>

</html>