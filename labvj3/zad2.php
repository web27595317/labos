<?php
$predmeti = array(
    0 => 'Mikroračunala',
    1 => 'Programsko inžinjerstvo',
    2 => 'Operacijski sustavi',
    3 => 'Osnove programskog jezika JAVA',
    4 => 'C# programiranje',
    5 => 'Web programiranje 2',
    6 => 'Tehnički engleski 4'
);
$ocjene = array(
    5 => 'odličan',
    4 => 'vrlo dobar',
    3 => 'dobar',
    2 => 'dovoljan',
    1 => 'nedovoljan'
);
$korisnici = array(
    0 => 'ilisjak',
    1 => 'ebirt',
    2 => 'bkovacevic',
    3 => 'ajungic'
)
    ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="get">
        <label for="username">Unesite vaš username: </label>
        <input type="text" id="username" name="username">
        <br><select name="predmeti" id="predmeti">
            <?php
            foreach ($predmeti as $key => $value) {
                echo '<option value="' . $key . '">' . $value . '</option>';
            }
            ?>
        </select>
        <br>
        <?php

        foreach ($ocjene as $key => $value) {
            echo '<input type="radio" name="ocjena" value="' . $key . '"> ' . $value . '<br>';
        }
        ?>
        <br><button type="submit" name="submit">Pošalji</button>
    </form>

    <script>
        function showAlert(username, ocjena, predmet) {
            if (username && ocjena && predmet) {
                alert("Pozdrav, " + username + ", dobili ste " + ocjena + " iz " + predmet + "!");;
                return 0;
            }
            else if (!username) {
                alert("Molimo unesite username");
                return 0;
            }
            else if (!ocjena) {
                alert("Molimo unesite ocjenu");
                return 0;
            }
            else if (!predmet) {
                alert("Molimo unesite predmet");
                return 0;
            }
            else if (!username && !ocjena) {
                alert("Molimo unesite username");
                return 0;
            }
        }
    </script>
    <?php
    if (isset($_GET['submit'])) {
        $username = $_GET['username'];
        $ocjena = $ocjene[$_GET['ocjena']];
        $predmet = $predmeti[$_GET['predmeti']];
        if (in_array($username, $korisnici)) {
            echo '<script>showAlert("' . $username . '", "' . $ocjena . '", "' . $predmet . '")</script>';
        } else {
            echo '<script>alert("Username ne postoji")</script>';
        }
    }
    ?>
</body>

</html>