<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    function getInsert($data, $table)
    {
        $columns = implode(', ', array_keys($data));
        $values = "'" . implode("', '", array_values($data)) . "'";
        $query = "INSERT INTO $table ($columns) VALUES ($values)";
        return $query;
    }
    if (!empty($_POST)) {
        $data = array(
            'ime' => $_POST['ime'],
            'prezime' => $_POST['prezime'],
            'OIB' => $_POST['OIB'],
            'email' => $_POST['email'],
            'godiste' => $_POST['godiste']
        );
        $query = getInsert($data, 'korisnici');
        echo $query;
    }

    ?>

</body>

</html>