<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    function arrayCount($array)
    {
        $count = 0;
        foreach ($array as $element) {
            $count++;
        }
        return $count;
    }
    function arrayKey($array)
    {
        $output = array();
        foreach ($array as $key => $value) {
            $output[$key] = null;
        }
        return $output;
    }
    function arrayValue($array)
    {
        $output = array();
        foreach ($array as $key => $value) {
            $output[$value] = null;
        }
        return $output;
    }
    function arrayHeader($array)
    {
        $header = '<tr>';
        foreach ($array as $key => $value) {
            $header .= '<th>' . $key . '</th>';
        }
        $header .= '</tr>';
        return $header;
    }
    function arrayRow($array)
    {
        $row = '<tr>';
        foreach ($array as $value) {
            $row .= '<td>' . $value . '</td>';
        }
        $row .= '</tr>';
        return $row;
    }
    
    $gradovi = array(
        'Hrvatska' => 'Zagreb',
        'Njemačka' => 'Berlin',
        'Austrija' => 'Beč',
        'Francuska' => 'Pariz',
        'Mađarska' => 'Budimpešta'
    );
    $korisnik = [
        "ime" => "Mario",
        "prezime" => "Kušević",
        "OIB" => 65822145815,
        "email" => "mkusevic@vub.hr",
        "godiste" => 1999
    ];

    echo "<pre>";
    print_r($gradovi);

    echo "Broj elementa: " . arrayCount($gradovi) . "<br>";

    echo "arrayKey(): <br>";
    $outputKeys = arrayKey($gradovi);
    print_r($outputKeys);

    echo "arrayValue(): <br>";
    $outputValues = arrayValue($gradovi);
    print_r($outputValues);

    echo "<pre>";
    echo 'arrayHeader() <table>' . arrayHeader($korisnik) . '</table>';
    echo 'arrayRow() <table>' . arrayRow($korisnik) . '</table>';
        ?>

</body>

</html>