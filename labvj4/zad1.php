<?php
function getHTMLpage($bodyContent)
{
    $html = '<!DOCTYPE html>';
    $html .= '<html>';
    $html .= '<head>';
    $html .= '<title>My Webpage</title>';
    $html .= '<style>';
    $html .= '/* CSS styles here */';
    $html .= '</style>';
    $html .= '</head>';
    $html .= '<body>';
    $html .= $bodyContent;
    $html .= '</body>';
    $html .= '</html>';

    return $html;
}

$bodyContent = '<b>Hello World</b>';
$htmlPage = getHTMLpage($bodyContent);

echo $htmlPage;
?>